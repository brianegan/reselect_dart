# Changelog

## 0.2.0

- No longer export the memoizing functions
- Use community `memoize` package instead

## 0.1.1

- Fix link to repo in pubspec

## 0.1.0

- Initial version, includes the `Selector` typedef and `createSelector1` through `createSelector10` as mechanisms to create and combine memoized selectors. 
- Includes docs
- Includes tests