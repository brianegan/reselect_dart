# Contributing

Contributions are very welcome in many forms :)

## Issues

If you find a problem with this library, please [file an issue on Gitlab](https://gitlab.com/brianegan/reselect_dart/issues).

## Merge Requests

If you would like to fix a bug or make an improvement, please [file a merge request](https://gitlab.com/brianegan/reselect_dart/merge_requests) explaining your changes!

For larger changes, such as a breaking API change, please submit an Issue or 
"Work In Progress" Merge request that gives an overview of the changes you'd
like to make and the reasoning behind it!

### Code Hygiene

  * Ensure your code is properly formatted with `dartfmt`
  * Please ensure tests are up to date
  * Verify `dartanalyzer` isn't throwing any warning or errors/